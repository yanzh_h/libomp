Name:          libomp
Version:       7.0.1
Release:       2
Summary:       OpenMP runtime for clang

License:       NCSA
URL:           http://openmp.llvm.org	
Source0:       http://%{?rc_ver:pre}releases.llvm.org/%{version}/%{?rc_ver:rc%{rc_ver}}/openmp-%{version}%{?rc_ver:rc%{rc_ver}}.src.tar.xz
Source1: runtest.sh

Patch0:        0001-CMake-Make-LIBOMP_HEADERS_INSTALL_PATH-a-cache-varia.patch

BuildRequires: cmake, elfutils-libelf-devel perl, perl-Data-Dumper, perl-Encode libffi-devel
Requires:      elfutils-libelf%{?isa}


%description
A OpenMP runtime for clang, subproject of LLVM, which contains the 
components to build an executable OpenMP program. 

%package devel
Summary:       OpenMP header files and development libs
Requires:      clang-devel%{?isa} = %{version}
%description devel
OpenMP header files and development libs

%package test
Summary:       OpenMP regression tests
Requires:      %{name}%{?isa} = %{version}
Requires:      %{name}-devel%{?isa} = %{version}
Requires:      clang, llvm, gcc, gcc-c++, python3-lit

%description test
OpenMP regression tests

%prep
%autosetup -n openmp-%{version}%{?rc_ver:rc%{rc_ver}}.src -p1

%build
install -d  _build
cd _build

%cmake .. -DLIBOMP_INSTALL_ALIASES=OFF \
       -DLIBOMP_HEADERS_INSTALL_PATH:PATH=%{_libdir}/clang/%{version}/include \
       -DOPENMP_LIBDIR_SUFFIX=64 

%make_build

%install
%make_install -C _build

%global libomp_srcdir %{_datadir}/libomp/src
%global libomp_testdir %{libomp_srcdir}/runtime/test
%global gcc_lit_cfg %{buildroot}%{libomp_testdir}/gcc.site.cfg
%global clang_lit_cfg %{buildroot}%{libomp_testdir}/clang.site.cfg

install -d %{buildroot}%{libomp_srcdir}/runtime
cp -R runtime/src  %{buildroot}%{libomp_srcdir}/runtime
cp -R runtime/test  %{buildroot}%{libomp_srcdir}/runtime

ln -s %{_libdir}/clang/%{version}/include/omp.h %{buildroot}%{libomp_testdir}/omp.h
ln -s %{_libdir}/clang/%{version}/include/ompt.h %{buildroot}%{libomp_testdir}/ompt.h
ln -s %{_libdir}/libomp.so %{buildroot}%{libomp_testdir}/libgomp.so

echo "import tempfile" > %{gcc_lit_cfg}
cat _build/runtime/test/lit.site.cfg >> %{gcc_lit_cfg}

sed -i '
s~\(config.test_filecheck = \)""~\1"%{_libdir}/llvm/FileCheck"~; 
s~\(config.omp_header_directory = \)"[^"]\+"~\1"%{_includedir}"~;
s~\(config.libomp_obj_root = \)"[^"]\+"~\1tempfile.mkdtemp()[1]~;
s~\(lit_config.load_config(config, \)"[^"]\+"~\1"%{libomp_testdir}/lit.cfg"~;
s~\(config.test_c_compiler = \)"[^"]\+"~\1"%{_bindir}/gcc"~;
s~\(config.test_cxx_compiler = \)"[^"]\+"~\1"%{_bindir}/g++"~;
s~\(config.library_dir = \)"[^"]\+"~\1"%{libomp_testdir}"~;
' %{gcc_lit_cfg} 

cp %{gcc_lit_cfg} %{clang_lit_cfg}
sed -i '
s~\(config.test_compiler_features = \)\[[^\[]\+]~\1["clang"]~;
s~\(config.test_c_compiler = \)"[^"]\+"~\1"%{_bindir}/clang"~;
s~\(config.test_cxx_compiler = \)"[^"]\+"~#1"%{_bindir}/clang++"~;
s~\(config.library_dir = \)"[^"]\+"~\1"%{_libdir}"~
' %{clang_lit_cfg}


install -m 0755 %{SOURCE1} %{buildroot}%{_datadir}/libomp


%files
%{_libdir}/libomp.so
%{_libdir}/libomptarget.so
%{_libdir}/libomptarget.rtl.%{_arch}.so
%exclude %{_libdir}/clang/%{version}/include/omp.h
%exclude %{_libdir}/clang/%{version}/include/ompt.h

%files devel
%{_libdir}/clang/%{version}/include/omp.h
%{_libdir}/libomp.so
%{_libdir}/libomptarget.so
%{_libdir}/libomptarget.rtl.%{_arch}.so

%files test
%{_datadir}/libomp

%changelog
* Sat Dec 28 2019 chenzhenyu <chenzhenyu13@huawei.com> - 7.0.1-2
- update from "aarch64" to "%{_arch}" for line 93 & line 101  

* Sun Dec 1 2019  jiaxiya <jiaxiyajiaxiya@168.com> - 7.0.1-2 
- Package init
